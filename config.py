#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

config = {
  "port": os.getenv('PORT', '80'),
  "host": os.getenv('HOST', '0.0.0.0'),
  "debug": bool(os.getenv('DEBUG', 'True'))
#  "bind": os.getenv('BIND', '{}:{}'.format(host,port)),
}
