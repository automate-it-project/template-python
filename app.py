#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask
from requests import get

from config import config


app = Flask(__name__)



@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def proxy(path):
  return get(
    '{}{}'.format('https://google.com/',path)
  ).content


if __name__ == '__main__':
  app.run(
    port=config['port'],
    host=config['host'],
    debug=config['debug']
  )
